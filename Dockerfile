FROM alpine:3.7

RUN apk update && \
    apk upgrade && \
    apk --update add \
      build-base libstdc++ tzdata ca-certificates openssh-client \
      ruby ruby-dev ruby-irb ruby-rake ruby-io-console ruby-bigdecimal ruby-json \
    && \
    echo 'gem: --no-document' > /etc/gemrc && \
    gem install chef:12.19.36 chef-vault:3.0.3 && \
    apk --purge del build-base ruby-dev